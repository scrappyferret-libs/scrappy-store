--- Required libraries.
local store
local json = require( "json" )

-- Localised functions.
local getInfo = system.getInfo

-- Localised values.

-- Static values.

--- Class creation.
local library = {}

--- Initialises this Scrappy library.
-- @param params The params for the initialisation.
function library:init( params )

	-- Store out the params, if any
	self._params = params or {}

	-- List of registered products
	self._registeredProducts = self._params.products or {}

	-- List of valid products
	self._validProducts = {}

	-- List of invalid products
	self._invalidProducts = {}

	-- List of purchased products
	self._purchasedProducts = {}

	-- Load in the purchased products
	self:load()

	-- Get the current target app store
	local appStore = self:getTargetAppStore()

	if appStore == "apple" then
		store = require( "plugin.apple.iap" )
	elseif appStore == "google" then
 		store = require( "plugin.google.iap.billing.v2" )
	elseif appStore == "amazon" then
		store = require( "plugin.amazon.iap" )
	else
		print( "In-app purchases are not available for this platform - ", appStore )
	end

	native.setActivityIndicator( false )

	-- Do we have a store library
	if store then

		local transactionListener = function( event )

		    local transaction = event.transaction

		   if event.name == "init" then

			   if transaction and transaction.isError then

			   else
				   if not system.getInfo( "platform" ) == "macos" then
					   self:requestReceipt()
				   end

				   self:loadProducts()
				   Runtime:dispatchEvent{ name = "store", state = "initiated" }
			   end

		   -- Store transaction event
		   elseif event.name == "storeTransaction" then

				local state = transaction.state

				if transaction.isError then

			    else

				   if state == "failed" then

				   elseif state == "cancelled" then

				   elseif state == "purchased" or state == "restored" then

					  self._purchasedProducts[ transaction.productIdentifier ] = transaction

					   -- Save out the purchased products
					   self:save()

				   end

				   if transaction and transaction.productIdentifier then
					   Runtime:dispatchEvent{ name = "store", state = state, transaction = transaction, product = self:getProductName( transaction.productIdentifier ) }
				   end



			   end

			   store.finishTransaction( transaction )

			   native.setActivityIndicator( false )

		   end

		end

		-- Initialize store
		if self:getTargetAppStore() == "apple" then

			-- Loop through our registered products
			for name, ids in pairs( self._registeredProducts or {} ) do

				-- Loop through each store
				for store, id in pairs( ids ) do

					-- Is this an apple product and is the id a table?
					if store == "apple" and type( id ) == "table" then

						-- Are we on OSX?
						if Scrappy.Device:isOSX() then

							-- Adjust the product id to just the osx one
							self._registeredProducts[ name ][ store ] = id[ "osx" ]

						-- Or is this iOS?
						elseif Scrappy.Device:isIOS() then

							-- Adjust the product id to just the ios one
							self._registeredProducts[ name ][ store ] = id[ "ios" ]

						end

					end

				end

			end

			store.init( "apple", transactionListener )

		else
			store.init( transactionListener )
		end

	end

end

function library:getTargetAppStore()
	return getInfo( "targetAppStore" )
end

function library:isActive()
	return store and store.isActive
end

function library:canMakePurchases()
	return store and store.canMakePurchases
end

function library:canLoadProducts()
	return store and store.canLoadProducts
end

function library:purchase( name )

	if self:isActive() and self:canMakePurchases() then

		-- First get the id for this product
		local id = self:getProductID( name )

		-- Do we have one?
		if id then

			native.setActivityIndicator( true )

			-- Then try to purchase it
			store.purchase( id )

		end

	end

end

function library:isPurchased( name )

	-- First get the id for this product
	local id = self:getProductID( name )

	-- Do we have one?
	if id then

		-- Then return if it's been purchased or not
		return self._purchasedProducts[ id ]

	end

end

function library:loadProducts()

	if self:canLoadProducts() or self:getTargetAppStore() == "amazon" then

		local productIdentifiers = {}

		local productListener = function( event )

			for i = 1, #( event.products or {} ), 1 do
				self._validProducts[ #self._validProducts + 1 ] = event.products[ i ]
		    end

			for i = 1, #event.invalidProducts, 1 do
				self._invalidProducts[ #self._invalidProducts + 1 ] = event.invalidProducts[ i ]
		    end

			if self:getTargetAppStore() ~= "apple" then
				self:restore()
			end

		end

		for k, _ in pairs( self._registeredProducts ) do
			productIdentifiers[ #productIdentifiers + 1 ] = self:getProductID( k )
		end

		store.loadProducts( productIdentifiers, productListener )

	end

end

function library:getProductName( id )

	for k, v in pairs( self._registeredProducts ) do

		-- Is the name a list of store-specific IDs?
		if type( v ) == "table" then

			-- Does the id match the name for this store?
			if self._registeredProducts[ k ][ self:getTargetAppStore() ] == id then

				-- Then return the name
				return k

			end

		else --Otherwise

			-- Does the stored ID match the passed in one?
			if v == id then

				-- Then return the name
				return k

			end

		end

	end

	-- Do we have a list of products and this product name?
	if self._registeredProducts and self._registeredProducts[ name ] then

		-- Is the name a list of store-specific IDs?
		if type( self._registeredProducts[ name ] ) == "table" then

			-- Then return the ID for the current store
			return self._registeredProducts[ name ][ self:getTargetAppStore() ]

		else --Otherwise

			-- Just return the ID
			return self._registeredProducts[ name ]

		end

	end

end

function library:getProductID( name )

	-- Do we have a list of products and this product name?
	if self._registeredProducts and self._registeredProducts[ name ] then

		-- Is the name a list of store-specific IDs?
		if type( self._registeredProducts[ name ] ) == "table" then

			-- Then return the ID for the current store
			return self._registeredProducts[ name ][ self:getTargetAppStore() ]

		else --Otherwise

			-- Just return the ID
			return self._registeredProducts[ name ]

		end

	end

end

function library:requestReceipt( onComplete )

	-- Are we on an apple store and we're currently active?
	if self:getTargetAppStore() == "apple" and self:isActive() then

		local listener = function( event )

			if onComplete and type( onComplete ) == "function" then
				onComplete( store.receiptAvailable() )
			end

		end

		store.receiptRequest( listener )

	end

end

function library:getReceipt( mode )

	-- Are we on an apple store and we're currently active?
	if self:getTargetAppStore() == "apple" and self:isActive() then

		-- Do we have a receipt available?
		if store.receiptAvailable() then

			-- Was a mode passed in?
			if mode then

				-- Was it raw?
				if mode == "raw" then

					-- Then recursively call again with no mode as I'm lazy.
					return self:getReceipt()

				-- Base 64?
				elseif mode == "base64" then

					-- Return the base 64 data
					return store.receiptBase64Data()

				-- Decrypted?
				elseif mode == "decrypted" then

					-- Return the decrypted data
					return store.receiptDecrypted()

				end

			else -- Otherwise no mode

				-- So return the raw data
				return store.receiptRawData()

			end

		end

	end

end

function library:getProductInfo( name )

	-- First get the id for this product
	local id = self:getProductID( name )

	-- Do we have one?
	if id then

		-- Then return the info, if it's a valid product
		return self._validProducts[ id ]

	end

end

function library:restore()
	if self:isActive() then
		store.restore()
    	native.setActivityIndicator( true )
	end
end

function library:verify( originalIdentifier )

    local iaps = self:getReceipt( "decrypted" ).in_app

    for i = 1, #iaps, 1 do

        local iap = iaps[ i ]

        if iap.original_transaction_id == originalIdentifier then
            return true
        end

    end

    return false

end

function library:save()

	local path = system.pathForFile( "store.dat", system.DocumentsDirectory )

	-- Open the file handle
	local file, errorString = io.open( path, "w" )

	if not file then

	else
	    -- Write data to file
	    file:write( json.encode( self._purchasedProducts or {} ) )

	    -- Close the file handle
	    io.close( file )

	end

	file = nil

end

function library:load()

		-- Path for the file to read
	local path = system.pathForFile( "store.dat", system.DocumentsDirectory )

	-- Open the file handle
	local file, errorString = io.open( path, "r" )

	if not file then

	else
	    -- Read data from file
	    local contents = file:read( "*a" )

	   	self._purchasedProducts = json.decode( contents or "" ) or {}

	    -- Close the file handle
	    io.close( file )

	end

	file = nil

end

--- Destroys this library.
function library:destroy()

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Store library
if not Scrappy.Store then

	-- Then store the library out
	Scrappy.Store = library

end

-- Return the new library
return library
